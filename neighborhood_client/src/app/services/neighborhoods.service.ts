import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {INeighborhood} from '../model/INeighborhood';
import { ICity } from '../model/ICity'; 
import { UserRequest } from '../model/UserRequest';
 
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable()
export class NeighborhoodsService {
 
    constructor(private _http:HttpClient) {}
 
    private _gmapNeighborhoodUrl ='/api/gmap/allneighborhoods';

    private _osmNeighborhoodUrl ='/api/osm/allneighborhoods';

    private _osmCitiesUrl ='/api/osm/cities';

    private _osmNewUserRequestUrl ='/api/osm/user/new/request';


    getGMapNeighborhoods(): Observable<INeighborhood[]> {
        return this._http.get(this._gmapNeighborhoodUrl)
        .do(data => console.log("All: " + JSON.stringify(data)))
        .catch(this.handleError);
    }

    getOSmNeighborhoods(): Observable<INeighborhood[]> {
        return this._http.get(this._osmNeighborhoodUrl)
       // .map((response:Response) => <INeighborhood[]>response.json())
        .do(data => console.log("All: " + JSON.stringify(data)))
        .catch(this.handleError);
    }

    getCities(): Observable<ICity[]> {
        return this._http.get(this._osmCitiesUrl)
       // .map((response:Response) => <INeighborhood[]>response.json())
        .do(data => console.log("All: " + JSON.stringify(data)))
        .catch(this.handleError);
    }

    postNewUserRequest(userRequest: UserRequest): Observable<UserRequest> {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json'
            })
        };

        return this._http.post<UserRequest>(this._osmNewUserRequestUrl, userRequest, httpOptions)
        .do(data => console.log("User Request: " + JSON.stringify(data)))
        .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json() || 'server error');
    }
}