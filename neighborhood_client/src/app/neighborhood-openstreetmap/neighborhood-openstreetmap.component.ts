import { Component, OnInit } from '@angular/core';
import {NeighborhoodsService} from '../services/neighborhoods.service';
import { MapService } from '../services/map.service';
import { INeighborhood } from '../model/INeighborhood'; 
declare let L;

@Component({
  selector: 'app-neighborhood-openstreetmap',
  templateUrl: './neighborhood-openstreetmap.component.html',
  styleUrls: ['./neighborhood-openstreetmap.component.css']
})
export class NeighborhoodOpenstreetmapComponent implements OnInit {
  
  constructor(private _neighService: NeighborhoodsService, private _mapService: MapService) {
  }

  ngOnInit() {
    this._neighService.getCities().subscribe (
      data => { 
        this._mapService.init(data[0].lat,  data[0].lon)
      },
      err => console.error(err),
      () => console.log('done loading city data')
    );

    this._neighService.getOSmNeighborhoods().subscribe (
        data => { 
          data.forEach( (neighborhood : INeighborhood) => {
            this._mapService.addPolygon(neighborhood, false)  
          });
        },
      err => console.error(err),
      () => console.log('done loading polygons data')
    );

  }

}
