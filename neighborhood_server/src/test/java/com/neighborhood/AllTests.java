package com.neighborhood;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.neighborhood.controllers.NeighborhoodControllerTests;
import com.neighborhood.converters.GeoJsonConverterTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({
   GeoJsonConverterTests.class,
   NeighborhoodControllerTests.class
})
public class AllTests {

}
