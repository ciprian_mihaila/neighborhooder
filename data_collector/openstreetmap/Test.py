from openstreetmap.InputParser import InputParser
from openstreetmap.OSMDataExtractor import OSMDataExtractor
from openstreetmap.MongoDbSave import MongoDbSave
from geopy.point import Point


input = InputParser()
neighbor_list = input.read_zone_and_suburbies("../zone_subzone_imobiliare_format")

bucharest_bound = [Point(44.5445, 25.9600), Point(44.3209, 26.2766)]
extractor = OSMDataExtractor(bucharest_bound)

database = MongoDbSave('neighborhoods')

for neighbour in neighbor_list:
   suburbies = extractor.get_zones(neighbour[0], neighbour[1:])
   database.insert_multiple(suburbies)
