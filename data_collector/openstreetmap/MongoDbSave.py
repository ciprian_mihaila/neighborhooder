from pymongo import MongoClient


class MongoDbSave:
    HOST = 'localhost'
    PORT = 27017
    DB = 'neighbour-database'

    def __init__(self, collection):
        client = MongoClient(self.HOST, self.PORT)
        db = client[self.DB]
        self.collection = db[collection]
        if self.collection.count() > 0:
            self.collection.drop()

    def insert_one(self, object):
        self.collection.insert_one(object)

    def insert_multiple(self, objects):
        for obj in objects:
            self.insert_one(obj)
