import { Component, OnInit } from '@angular/core';
import {NeighborhoodsService} from '../services/neighborhoods.service';

@Component({
  selector: 'app-neighborhood-legend',
  templateUrl: './neighborhood-legend.component.html',
  styleUrls: ['./neighborhood-legend.component.css']
})
export class NeighborhoodLegendComponent implements OnInit {
  public neighborhoods;

  constructor(private _neighService: NeighborhoodsService) { }

  ngOnInit() {
    this._neighService.getOSmNeighborhoods().subscribe(
      data => { 
        this.neighborhoods = data
      },
    err => console.error(err),
    () => console.log('done loading foods')
  );
  }

}
