# City Neighborhoods Centralizer (Neighborhooder)
The application is a pet project that has the purpose to centralize the neighborhoods for multiple cities. For the moment, the suburbs from two cities: Bucuresti and Cluj-Napoca are displayed on a OpenStreetMap. An external user can perform a request for a desired city. 
The application has 3 components:

*  data-collector (used for retrieving suburb data from OpenStreetMap Nominatim and Wikipedia)
    * [Get suburbies list](data_collector/api.md) : `/get/suburbies`
    * [Get city details](data_collector/api.md) : `/get/citydetails`
*  neighborhood_server (spring boot application used for serving required data)
*  neighborhood_client (Angular 5 - application standalone GUI)

### Technologies
- Java 8, Spring Boot 2, Mockito
- Mongo DB
- Angular 5, Bootstrap 4, Leaflet JS
- Python (Flask, GeoPy)
- Docker

### Gif Demo
![](neighborhooder.gif)