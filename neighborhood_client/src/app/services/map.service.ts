import { Injectable } from "@angular/core";
import { ICity } from "../model/ICity";
import { INeighborhood } from "../model/INeighborhood";

declare let L;

@Injectable()
export class MapService {

  private map: any

  init (lat, lon) {
    this.map = L.map('map', { zoomControl:false }).setView([lat, lon], 12);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
  }

  panTo (city: ICity) {
    this.map.panTo(new L.LatLng(city.lat,  city.lon))
  }

  addPolygon (neighborhood : INeighborhood, openPopup: boolean) {
    var polygon = L.polygon(
      [ neighborhood.polygon.coordinates ], 
      { 
        color: neighborhood.color, 
        opacity: 5.0, 
        fillOpacity: 0.7
      })
      .addTo(this.map)
      .bindPopup(neighborhood.name)

    if (openPopup) {
      polygon.on('mouseover',function(ev) {
        polygon.openPopup();
      });
    }
  }
  

}