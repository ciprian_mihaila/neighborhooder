package com.neighborhood.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("requests")
public class UserRequest {
	private String name;
	private String email;
	private String city;
	private List<String> neighborhoods;
	
	public UserRequest(String name, String email, String city, List<String> neighborhoods) {
		this.name = name;
		this.email = email;
		this.city = city;
		this.neighborhoods = neighborhoods;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public List<String> getNeighborhoods() {
		return neighborhoods;
	}
	public void setNeighborhoods(List<String> neighborhoods) {
		this.neighborhoods = neighborhoods;
	}
}
