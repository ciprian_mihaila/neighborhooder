package com.neighborhood.converters;

import java.util.ArrayList;
import java.util.List;

import com.neighborhood.model.Coordinate;
/**
 * convert from geojson array to different formats
 * 	-
 */
public class GeoJsonConverter {
	
	public static List<Coordinate> convertLatLngListToCoordinateList(List<ArrayList<String>> coordinates) {
		List<Coordinate> result = new ArrayList<Coordinate>();
		for (List<String> list : coordinates) {
			result.add(new Coordinate(String.valueOf(list.get(0)), String.valueOf(list.get(1))));
		}
		return result;
	}
	
	public static void convertLatLngListToLngLatList(List<ArrayList<String>> coordinates) {
		for (List<String> list : coordinates) {
			String first = String.valueOf(list.get(0));
			list.set(0, String.valueOf(list.get(1)));
			list.set(1, first);
		}
	}
}
