import { Component, OnInit } from '@angular/core';
import { LatLngLiteral } from '@agm/core';
import {NeighborhoodsService} from '../services/neighborhoods.service';

@Component({
  selector: 'app-neighborhood-map',
  templateUrl: './neighborhood-map.component.html',
  styleUrls: ['./neighborhood-map.component.css']
})
export class NeighborhoodMapComponent {

  lat: number = 44.439663;
  lng: number = 26.096306;

  public neighborhoods;
  
  paths: Array<Array<LatLngLiteral>> = [[
    { lng: 26.0553305,  lat:44.472429},{ lng:26.0580866,  lat:44.4708298},{ lng:26.0583634,  lat:44.4705622}
    ,{ lng:26.0591581,  lat:44.4690711},{ lng:26.058919,  lat:44.4686824},{ lng:26.0694303,  lat:44.4643745},
    { lng:26.0723881,  lat:44.4621403},{ lng:26.0740018,  lat:44.4634333},{ lng:26.0753723,  lat:44.4646832},
    { lng:26.0758792,  lat:44.4652834},{ lng:26.0775979,  lat:44.467137},{ lng:26.0747398,  lat:44.4679132},
    { lng:26.0736354,  lat:44.4683097},{ lng:26.0704923,  lat:44.4747581},{ lng:26.0553305,  lat:44.472429}
      ] ,
      [
        {lng:26.0723881,  lat:44.4621403},{lng:26.0800746,  lat:44.4560539},{lng:26.0863048,  lat:44.4568678},
        {lng:26.0864806,  lat:44.4653561},{lng:26.0859126,  lat:44.4658575},{lng:26.0853322,  lat:44.4662334},
        {lng:26.0775979,  lat:44.467137},{lng:26.0758792,  lat:44.4652834},{lng:26.0753723,  lat:44.4646832},
        {lng:26.0740018,  lat:44.4634333},{lng:26.0723881,  lat:44.4621403}
      ]]

  paths2: Array<LatLngLiteral> = [
    {lng:26.0723881,  lat:44.4621403},{lng:26.0800746,  lat:44.4560539},{lng:26.0863048,  lat:44.4568678},
    {lng:26.0864806,  lat:44.4653561},{lng:26.0859126,  lat:44.4658575},{lng:26.0853322,  lat:44.4662334},
    {lng:26.0775979,  lat:44.467137},{lng:26.0758792,  lat:44.4652834},{lng:26.0753723,  lat:44.4646832},
    {lng:26.0740018,  lat:44.4634333},{lng:26.0723881,  lat:44.4621403}
  ]

  constructor(private _neighService: NeighborhoodsService) {

  }
 

  ngOnInit() {
    this._neighService.getGMapNeighborhoods().subscribe(
              data => { 
                this.neighborhoods = data
              },
            err => console.error(err),
            () => console.log('done loading foods')
          );
  }

}
