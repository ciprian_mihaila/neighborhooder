package com.neighborhood.external.services;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.mongodb.BasicDBObject;
import com.neighborhood.converters.GeoJsonConverter;
import com.neighborhood.model.City;
import com.neighborhood.model.OsmNeighborhood;
import com.neighborhood.model.dto.NewNeighborhoodsRequestDTO;

@Component
@ConfigurationProperties(prefix = "collector")
public class DataCollectorRequest {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private RestTemplate restTemplate;
	
	private String host;
	
	private int port;
	
	public City getCityDetails(NewNeighborhoodsRequestDTO newNeighborhoodRequest) {
		String url = String.format(
				"http://%s:%d/get/citydetails?name=%s",
				getHost(), getPort(), newNeighborhoodRequest.getCity());
		
		String details = restTemplate.getForObject(url, String.class);
		City city = null;
		try {
			JSONObject jsonObject = new JSONObject(details);
			city = new City(newNeighborhoodRequest.getCity(), jsonObject.getString("lat"), jsonObject.getString("lon"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return city;
	}
	
	public JSONArray getZonesForCity(NewNeighborhoodsRequestDTO newNeighborhoodRequest) {
		String url = String.format(
				"http://%s:%d/get/suburbies",
				getHost(), getPort());
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Content-Type", "application/json");

		JSONArray resultJsonArray = null;
		
		try {
			JSONObject city = new JSONObject();
			city.put("name", newNeighborhoodRequest.getCity());
			city.put("locale", newNeighborhoodRequest.getLocale());
			city.put("districts", newNeighborhoodRequest.getDistricts());
			JSONObject requestJson = new JSONObject();
			requestJson.put("city", city);
			
			HttpEntity <String> httpEntity = new HttpEntity <String> (requestJson.toString(), httpHeaders);
			String response = restTemplate.postForObject(url, httpEntity, String.class);
			resultJsonArray = new JSONArray(response);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return resultJsonArray;
	}
	
	public List<OsmNeighborhood> prepareOsmNeighborhoodList(JSONArray array) {
		List<OsmNeighborhood> result = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			JSONObject json;
			try {
				json = array.getJSONObject(i);
				Bson bson = BasicDBObject.parse(json.toString());
				OsmNeighborhood neighborhood = mongoTemplate.getConverter().read(OsmNeighborhood.class, bson);
				List<ArrayList<String>> coordinates = neighborhood.getPolygon().getCoordinates();
				GeoJsonConverter.convertLatLngListToLngLatList(coordinates);
				result.add(neighborhood);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
