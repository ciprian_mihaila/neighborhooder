package com.neighborhood.repositories;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.neighborhood.model.OsmNeighborhood;

@Repository
public interface NeighborhoodRepository extends MongoRepository<OsmNeighborhood, String>, NeighborhoodRepositoryCustom {
	List<OsmNeighborhood> findByCity(String city);
}
