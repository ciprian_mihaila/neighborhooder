import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewNeighborhoodRequestComponent } from './new-neighborhood-request.component';

describe('NewNeighborhoodRequestComponent', () => {
  let component: NewNeighborhoodRequestComponent;
  let fixture: ComponentFixture<NewNeighborhoodRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewNeighborhoodRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewNeighborhoodRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
