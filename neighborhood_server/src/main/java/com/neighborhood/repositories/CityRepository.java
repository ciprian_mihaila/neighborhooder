package com.neighborhood.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.neighborhood.model.City;

public interface CityRepository extends MongoRepository<City, String> {
}
