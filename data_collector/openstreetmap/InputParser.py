class InputParser:

    def read_zone_and_suburbies(self, filepath):
        result_list = []
        with open(filepath) as fp:
            line = fp.readline()
            while line:
                split_result = [x.strip() for x in line.split(',')]
                result_list.append(split_result)
                line = fp.readline()
        return result_list

