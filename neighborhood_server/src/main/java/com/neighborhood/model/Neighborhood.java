package com.neighborhood.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

public class Neighborhood<T> {
	@Id
	public ObjectId _id;
	
	@Field("display_name")
	private String name;
	
	private String parent;
	
	private String color;
	
	private String city;
	
	private Polygon<T> polygon;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Polygon<T> getPolygon() {
		return polygon;
	}

	public void setPolygon(Polygon<T> polygon) {
		this.polygon = polygon;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
