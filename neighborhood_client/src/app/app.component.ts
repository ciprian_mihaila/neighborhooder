import { Component, Input } from '@angular/core';
import { NeighborhoodsService } from './services/neighborhoods.service';
import { MapService } from './services/map.service';
import { ICity } from './model/ICity';

declare let L;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  pageTitle: string = 'Neighborhooder'
  
  selectedCity: string
  cityList: ICity[] = [];

  constructor(private _neighService: NeighborhoodsService, private _mapService: MapService) {
  }

  ngOnInit() {
    this._neighService.getCities().subscribe (
        data => {
          this.cityList = data;
          this.selectedCity = data[0].name;
          this._mapService.panTo(data[0]);
        },
      err => console.error(err),
      () => console.log('done loading data')
    );
  }

  changeCity(city : ICity) {
    this.selectedCity = city.name;
    this._mapService.panTo(city);
  }

}
