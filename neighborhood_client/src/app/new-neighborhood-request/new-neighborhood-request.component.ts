import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserRequest } from '../model/UserRequest';
import { NeighborhoodsService } from '../services/neighborhoods.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-new-neighborhood-request',
  templateUrl: './new-neighborhood-request.component.html',
  styleUrls: ['./new-neighborhood-request.component.css']
})
export class NewNeighborhoodRequestComponent implements OnInit {

  userRequest: UserRequest
  userRequestForm: FormGroup;
  @ViewChild('closeBtn') closeBtn: ElementRef;

  
  constructor(private _neighService: NeighborhoodsService, private _fb: FormBuilder) {
  }

  ngOnInit() {
    this.userRequest = {
      _id: '',
      name: '',
      email: '',
      city: '',
      neighborhoods: ''
    };

    this.userRequestForm = this._fb.group({
      name: ['', Validators.required ],
      email: ['', Validators.required ],
      city: ['', Validators.required ],
      neighborhoods: ['']
    });
  }

  processRequest() {
    this._neighService.postNewUserRequest(this.userRequest).subscribe(value => {
      console.log(value);
    },
    err => {
      console.error('Error post new user request:', err.message);
    },
    () => {
      this.userRequestForm.reset();
      this.closeBtn.nativeElement.click();
      console.log(`We're done here!`);
    })
  }

}
