export interface INeighborhood {
    _id: string,
    name :string,
    parent:string,
    polygon: IPolygon,
    color: string
}

interface IPolygon {
    coordinates: string
}