package com.neighborhood.model;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("cities")
public class City {
	@Indexed(unique = true)
	private String name;
	private String lat;
	private String lon;
	
	public City(String name, String lat, String lon) {
		this.name = name;
		this.lat = lat;
		this.lon = lon;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
}
