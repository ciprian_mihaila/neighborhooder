package com.neighborhood.repositories;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

import com.neighborhood.model.OsmNeighborhood;

public class NeighborhoodRepositoryCustomImpl implements NeighborhoodRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<String> getCities() {
		Aggregation aggregation = newAggregation(group("city"), sort(Sort.Direction.ASC, "_id"));
		AggregationResults<CityResult> groupResults = mongoTemplate.aggregate(aggregation, OsmNeighborhood.class, CityResult.class);
		
		List<String> results = new ArrayList<String>();
		groupResults.getMappedResults().forEach(city -> {
			results.add(city.get_id());
		});
		
		return results;
	}
	
	private class CityResult {
		private String _id;

		public String get_id() {
			return _id;
		}
	}

}
