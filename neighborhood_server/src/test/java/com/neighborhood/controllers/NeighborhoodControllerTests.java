package com.neighborhood.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.neighborhood.external.services.DataCollectorRequest;
import com.neighborhood.model.NewNeighborhoodsRequestDTO;
import com.neighborhood.model.OsmNeighborhood;
import com.neighborhood.model.Polygon;
import com.neighborhood.repositories.NeighborhoodRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(NeighborhoodController.class)
public class NeighborhoodControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private NeighborhoodRepository repository;

	@MockBean
	private DataCollectorRequest dataCollectorRequest;

	private OsmNeighborhood createOsmNeighborhood() {
		Polygon<ArrayList<String>> polygon = new Polygon<>();
		List<ArrayList<String>> coordinates = new ArrayList<ArrayList<String>>();
		coordinates.add(new ArrayList<String>() {
			{
				add("44.4703519");
				add("26.0438533");
			}
		});
		polygon.setCoordinates(coordinates);
		OsmNeighborhood osmNeighborhood = new OsmNeighborhood();
		osmNeighborhood.setPolygon(polygon);
		osmNeighborhood.setCity("Test");
		return osmNeighborhood;
	}

	private List<OsmNeighborhood> prepareRepository() {
		List<OsmNeighborhood> repo = new ArrayList<OsmNeighborhood>();
		OsmNeighborhood osmNeighborhood = createOsmNeighborhood();
		repo.add(osmNeighborhood);
		return repo;
	}

	@Test
	public void testGetAllOsmNeighborhoods() throws Exception {
		List<OsmNeighborhood> osmNeighborhoods = prepareRepository();
		when(repository.findAll()).thenReturn(osmNeighborhoods);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/osm/allneighborhoods"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].polygon.coordinates[0][0]", is("26.0438533")))
				.andExpect(jsonPath("$[0].polygon.coordinates[0][1]", is("44.4703519"))).andReturn();
	}

	@Test
	public void testGetOsmNeighborhoodsByCityName() throws Exception {
		List<OsmNeighborhood> osmNeighborhoods = prepareRepository();
		when(repository.findByCity("Test")).thenReturn(osmNeighborhoods);

		mockMvc.perform(MockMvcRequestBuilders.get("/api/osm/neighborhoods").param("city", "Test"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].polygon.coordinates[0][0]", is("44.4703519")))
				.andExpect(jsonPath("$[0].polygon.coordinates[0][1]", is("26.0438533")))
				.andExpect(jsonPath("$[0].city", is("Test"))).andReturn();
	}

	@Test
	public void testGetNewOsmNeighborhoods() throws Exception {
		NewNeighborhoodsRequestDTO content = new NewNeighborhoodsRequestDTO();
		content.setCity("Test");
		content.setLocale("TestLocale");
		
		JSONArray resultJsonArray = new JSONArray();
		
		List<OsmNeighborhood> osmNeighborhoods = prepareRepository();
		when(dataCollectorRequest.getZonesForCity(content)).thenReturn(resultJsonArray);
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(content);
		
		mockMvc.perform(MockMvcRequestBuilders
					.post("/api/osm/new/neighborhood")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(requestJson)
				)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")));
	}

}
