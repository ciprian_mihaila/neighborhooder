import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeighborhoodOpenstreetmapComponent } from './neighborhood-openstreetmap.component';

describe('NeighborhoodOpenstreetmapComponent', () => {
  let component: NeighborhoodOpenstreetmapComponent;
  let fixture: ComponentFixture<NeighborhoodOpenstreetmapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeighborhoodOpenstreetmapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeighborhoodOpenstreetmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
