# Data Collector API
## Get suburbies list

Used to retrieve a list of suburbies for a city

**URL** : `/get/suburbies`
**Method** : `POST`
**Auth required** : NO
**Data constraints**

```json
{ 
    "city": {
            "name": "[a city name]",
            "locale": "[a region locale]",
            "districts": "[an expected districts list]" (OPTIONAL)
    }
}
```
**Data example**

```json
{
    "city": {
            "name": "Bucuresti",
            "locale": "ro",
            "districts": ['Straulesti', 'Dristor']
    }
}
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
[
  {
    "address": {
      "country": "Rom\u00e2nia", 
      "country_code": "ro", 
      "state": "Municipiul Bucure\u0219ti", 
      "state_district": "Sector 6", 
      "suburb": "Drumul Taberei"
    }, 
    "boundingbox": [
      "44.4119007", 
      "44.4290172", 
      "26.0122541", 
      "26.0608389"
    ], 
    "city": "Bucure\u0219ti", 
    "color": "#4DD376", 
    "display_name": "Drumul Taberei, Sector 6, Municipiul Bucure\u0219ti, Rom\u00e2nia", 
    "importance": 0.509790987808389, 
    "lat": "44.42032135", 
    "lon": "26.033693243428", 
    "osm_id": 230426240, 
    "parent": null, 
    "place_id": 129863085, 
    "polygon": {
      "coordinates": [
        [
          26.0122541, 
          44.4242276
        ], 
        .....................		
        [
          26.0122541, 
          44.4242276
        ]
      ]
    }
  }
  ...................
]
```
## Error Response

**Condition** : If 'city' parameter is missing.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{'Request should contain city parameter'}
```
**Condition** : If 'name' parameter is missing.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{'Request should contain name parameter'}
```
**Condition** : If 'locale' parameter is missing.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{'Request should contain locale parameter'}
```
**Condition** : If no zones were found.

**Code** : `404 BAD REQUEST`

**Content** :

```json
{'No zones found in found in OSM'}
```

## Get city details
Used to retrieve city details

**URL** : `/get/citydetails`
**Method** : `GET`
**Auth required** : NO
**Data constraints**

```json
{  ?name=[city name] }
```
**Data example**

```json
{ get/citydetails?name=Cluj-Napoca }
```
## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "lat": "46.7782449",
    "lon": "23.606868698987"
}
```
## Error Response

**Condition** : If 'name' parameter is missing.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{'Request should contain city name'}
```
**Condition** : If no details were found.

**Code** : `404 BAD REQUEST`

**Content** :

```json
{'No details found in found in OSM'}
```