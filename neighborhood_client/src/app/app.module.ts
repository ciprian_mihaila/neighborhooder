import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http'; 
import { NeighborhoodOpenstreetmapComponent } from './neighborhood-openstreetmap/neighborhood-openstreetmap.component';
import {NeighborhoodsService} from './services/neighborhoods.service'
import 'rxjs/Rx';
import {
  MatListModule
} from '@angular/material';
import { MapService } from './services/map.service';
import { NewNeighborhoodRequestComponent } from './new-neighborhood-request/new-neighborhood-request.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const appRoutes: Routes = [
  { path: '', component: NeighborhoodOpenstreetmapComponent, pathMatch: 'full' },
  { path: '**', component: NeighborhoodOpenstreetmapComponent, redirectTo: '' }
]

@NgModule({
  declarations: [
    AppComponent,
    NeighborhoodOpenstreetmapComponent,
    NewNeighborhoodRequestComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes
    ),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBc2wO-VfcW69CFPW06tjfxb-Cbo1-lGdc'
    })
  ],
  providers: [NeighborhoodsService, MapService],
  bootstrap: [AppComponent]
})
export class AppModule { }
