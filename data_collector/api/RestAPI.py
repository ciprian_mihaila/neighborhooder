from flask import Flask, jsonify, abort, request
from openstreetmap.OSMDataExtractor import OSMDataExtractor
from openstreetmap.Utils import Utils
from wikipedia.WikipediaDistrictsExtractor import WikipediaDistrictsExtractor
import logging

app = Flask(__name__)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
if not len(logger.handlers):
    logger.addHandler(logging.StreamHandler())


def __init__(self, bound):
    self.bound = bound
    self.printParent = False

    self.logger = logging.getLogger(__name__)
    self.logger.setLevel(logging.INFO)
    if not len(self.logger.handlers):
        self.logger.addHandler(logging.StreamHandler())


def __check_request(data):
    if data is None:
        abort(400, {'Empty data'})
    if not 'city' in data:
        abort(400, {'Request should contain city parameter'})
    if not 'locale' in data['city']:
        abort(400, {'Request should contain locale parameters'})
    if not 'name' in data['city']:
        abort(400, {'Request should contain name parameter'})


@app.route('/get/suburbies', methods=['POST'])
def get_city_suburbies():
    data = request.get_json()
    __check_request(data)
    city_name = data['city']['name']
    locale = data['city']['locale']
    districts = []
    if 'districts' in data['city']:
        districts = data['city']['districts']

    wiki_extractor = WikipediaDistrictsExtractor()
    titles = wiki_extractor.get_district_list(locale, city_name)

    if len(titles) == 0:
        logger.warning("no districts found in wikipedia")
    else:
        logger.info("%s districts found in wikipedia for %s", len(titles), city_name)
        for title in titles:
            logger.info("Districts found: %s", title)

    clean_titles = Utils.remove_diacritics_from_list(titles)
    clean_districts = Utils.remove_diacritics_from_list(districts)
    search_zones = Utils.__concat_2lists_in_unique_list__(clean_titles, clean_districts)
    search_zones.sort()
    logger.info("Search zones: %s", search_zones)
    if len(search_zones) == 0:
        abort(404, {'No search zones available'})

    extractor = OSMDataExtractor(city_name)
    zones = extractor.get_zones(search_zones)

    if len(zones) == 0:
        logger.warning("No zones found in found in OSM")
        abort(404, {'No zones found in found in OSM'})
    else:
        logger.info("%s zones found for %s", len(zones), city_name)
        filtered_zones = Utils.filter_duplicates_in_json_list(zones)
        return jsonify(filtered_zones)


@app.route('/get/citydetails', methods=['GET'])
def get_city_details():
    city_name = request.args.get('name')
    if city_name is None:
        abort(400, {'Request should contain city name'})

    extractor = OSMDataExtractor(city_name)
    lat_long = extractor.get_city_lat_long()

    if lat_long is None:
        logger.warning("No details found in found in OSM")
        abort(404, {'No details found in found in OSM'})
    else:
        return jsonify(lat_long)


if __name__ == '__main__':
    app.run(debug=True)
