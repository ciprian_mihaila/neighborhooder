package com.neighborhood.repositories;

import java.util.List;

public interface NeighborhoodRepositoryCustom {
	public List<String> getCities();
}
