import requests
import logging


class WikipediaDistrictsExtractor:

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        if not len(self.logger.handlers):
            self.logger.addHandler(logging.StreamHandler())

    @staticmethod
    def prepare_district_list(districts, exclude_file):
        with open(exclude_file, encoding='utf8') as f:
            excluded_words = f.read().splitlines()
        result = []
        for dist in districts:
            for exclude in excluded_words:
                dist = dist.replace(exclude, "")

            dist = dist.strip()
            if dist is not "":
                result.append(dist)

        return result

    def wikipedia_category_request(self, locale_url, title):
        session = requests.Session()
        url = "https://{}.wikipedia.org/w/api.php".format(locale_url)
        params = {
            'action': "query",
            'list': "categorymembers",
            'cmtitle': title,
            'cmlimit': 100,
            'format': "json"
        }
        response = session.get(url=url, params=params)
        data = response.json()
        session.close()
        return data

    def get_titles_from_response(self, data, log=False):
        result = []
        for district in data['query']['categorymembers']:
            result.append(district['title'])

        if log:
            self.log_results(result)
        return result

    def get_search_category(self, locale, name):
        search_name = ''
        if locale == 'ro':
            search_name = 'Categorie:Cartiere din ' + name
        return search_name

    def get_district_list(self, locale_url, title, log=False):
        search_title = self.get_search_category(locale_url, title)
        if search_title is not '':
            data = self.wikipedia_category_request(locale_url, search_title)
            titles = self.get_titles_from_response(data, log)
            return self.prepare_district_list(titles,  '../wikipedia/resources/wikipedia_excludes_words')
        else:
            self.logger.warning("no districts found for: %s", title)
            return ''

    def log_results(self, data):
        for entry in data:
            self.logger.info(entry)

