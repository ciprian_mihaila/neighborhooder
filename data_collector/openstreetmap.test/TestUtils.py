import unittest
from openstreetmap.Utils import Utils


class MyTestCase(unittest.TestCase):
    def test__concat_unique_list(self):
        list1 = ['T1', 'T2', 'T3']
        list2 = ['T1', 'T2', 'T4']
        result = Utils.__concat_2lists_in_unique_list__(list1, list2)
        self.assertEqual(result, ['T1', 'T2', 'T3', 'T4'])

    def test_remove_diacritics(self):
        expected = "Crangasi"
        result = Utils.remove_diacritics("Crângași")
        self.assertEqual(expected, result)

    def test_remove_diacritics_from_list(self):
        test_list = ["Crângași", "Giulești", "Cărămidarilor"]
        expected_list = ["Crangasi", "Giulesti", "Caramidarilor"]
        result = Utils.remove_diacritics_from_list(test_list)
        self.assertEqual(result, expected_list)

    def test_filter_duplicates_in_json_list(self):
        test_zones = [
            {"osm_id": 240565544, "boundingbox": ["44.4470442", "44.4656307", "26.0863048", "26.1029733"],
             "lon": "26.0916255653698"},
            {"osm_id": 240565544, "boundingbox": ["44.4470442", "44.4656307", "26.0863048", "26.1029733"],
             "lon": "26.0916255653698"},
            {"osm_id": 230444701, "boundingbox": ["44.4122112", "44.4258985", "26.1342202", "26.1503872"],
             "lon": "26.1416228228786"},
            {"osm_id": 230426240, "boundingbox": ["44.4119007", "44.4290172", "26.0122541", "26.0608389"],
             "lon": "26.033693243428"},
            {"osm_id": 596528297, "boundingbox": ["44.4823095", "44.4995057", "26.0409736", "26.0719279"],
             "lon": "26.0574785308939"}
        ]
        expected_zones = [
            {"osm_id": 240565544, "boundingbox": ["44.4470442", "44.4656307", "26.0863048", "26.1029733"],
             "lon": "26.0916255653698"},
            {"osm_id": 230444701, "boundingbox": ["44.4122112", "44.4258985", "26.1342202", "26.1503872"],
             "lon": "26.1416228228786"},
            {"osm_id": 230426240, "boundingbox": ["44.4119007", "44.4290172", "26.0122541", "26.0608389"],
             "lon": "26.033693243428"},
            {"osm_id": 596528297, "boundingbox": ["44.4823095", "44.4995057", "26.0409736", "26.0719279"],
             "lon": "26.0574785308939"}
        ]
        result = Utils.filter_duplicates_in_json_list(test_zones)
        self.assertEqual(len(result), 4)
        for expect in expected_zones:
            self.assertTrue(expect in expected_zones)


if __name__ == '__main__':
    unittest.main()
