import unittest
from wikipedia.WikipediaDistrictsExtractor import WikipediaDistrictsExtractor


class MyTestCase(unittest.TestCase):

    def test_country_districts(self):
        wiki_extractor = WikipediaDistrictsExtractor()
        data = wiki_extractor.wikipedia_category_request("ro", "Categorie:Cartiere_în_România")
        titles = wiki_extractor.get_titles_from_response(data, True)
        self.assertTrue("Categorie:Cartiere din București" in titles)
        self.assertTrue("Categorie:Cartiere din Brașov" in titles)
        self.assertTrue("Categorie:Cartiere din Cluj-Napoca" in titles)

    def test_country_city_districts(self):
        wiki_extractor = WikipediaDistrictsExtractor()
        data = wiki_extractor.wikipedia_category_request("ro", "Categorie:Cartiere din Cluj-Napoca")
        titles = wiki_extractor.get_titles_from_response(data, True)
        self.assertTrue("Cartierul Mărăști din Cluj-Napoca" in titles)
        self.assertTrue("Cartierul Gruia din Cluj-Napoca" in titles)
        self.assertTrue("Cluj-Mănăștur" in titles)

    def test_prepare_district_list(self):
        titles = ["Format:București", "Lista cartierelor din București", "Aviatorilor (cartier)",
                "Berceni (cartier)", "Cartierul Mărăști din Cluj-Napoca"]
        parsed = WikipediaDistrictsExtractor.prepare_district_list(titles, '../wikipedia/resources/wikipedia_excludes_words')
        self.assertEqual(len(parsed), 3)
        self.assertTrue("Aviatorilor" in parsed)
        self.assertTrue("Berceni" in parsed)
        self.assertTrue("Mărăști" in parsed)

    def test_get_district_list(self):
        wiki_extractor = WikipediaDistrictsExtractor()
        districts = wiki_extractor.get_district_list("ro", "București", True)
        self.assertTrue('Aviatorilor' in districts)
        self.assertTrue('Bucureștii Noi' in districts)
        self.assertTrue('Colentina' in districts)

    def test_get_district_list_empty(self):
        wiki_extractor = WikipediaDistrictsExtractor()
        districts = wiki_extractor.get_district_list("test", "TEST", True)
        self.assertEqual(len(districts), 0)

    def test_get_search_category(self):
        wiki_extractor = WikipediaDistrictsExtractor()
        category = wiki_extractor.get_search_category('ro', 'TEST')
        self.assertEqual(category, 'Categorie:Cartiere din TEST')

    def test_get_search_category_empty(self):
        wiki_extractor = WikipediaDistrictsExtractor()
        category = wiki_extractor.get_search_category('asd', 'TEST')
        self.assertEqual(category, '')


if __name__ == '__main__':
    unittest.main()
