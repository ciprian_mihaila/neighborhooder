from geopy.geocoders import Nominatim
from geopy.point import Point
import logging
from openstreetmap.Utils import Utils


class OSMDataExtractor:
    USER_AGENT = "data Collector user agent"
    GEOJSON_GEOMETRY = "geojson"
    TIMEOUT = 15
    TYPES = ["suburb", "administrative", "neighbourhood"]

    def __init__(self, city_name):
        self.city_name = city_name
        self.printParent = False

        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        if not len(self.logger.handlers):
            self.logger.addHandler(logging.StreamHandler())

    ''''
        get zone (suburb, administrative, neighborhood) location 
        properties (geocode, address, etc) for 
            name - zone name
            parent - parent of zone if the zones are organized in groups
    '''
    def get_zone(self, name, parent):
        return_location = None
        bound = self.__get_bound__()
        geolocator = Nominatim(user_agent=self.USER_AGENT,
                               bounded=(bound is not None),
                               view_box=bound,
                               timeout=self.TIMEOUT)
        locations = geolocator.geocode(name, addressdetails=True, geometry=self.GEOJSON_GEOMETRY,
                                       exactly_one=False, extratags=True)

        if locations is not None:
            for location in locations:
                if hasattr(location, 'raw'):
                    raw_data = location.raw
                    if raw_data['type'] in self.TYPES and raw_data['geojson']['type'] == "Polygon":
                        return_location = raw_data
                        return_location['parent'] = parent

                        if parent is not None and not self.printParent:
                            self.logger.info(">>>>>>>>>>>>> %s", parent)
                            self.printParent = True

                        self.logger.info("cartier %s", name)
                        self.logger.info(raw_data)
                    else:
                        self.logger.warning("other type found for: %s %s", name, raw_data['type'])
                else:
                    self.logger.warning("no location raw found for: %s", name)
        else:
            self.logger.warning("no entry found for: %s", name)

        return return_location

    def __prepareLocation__(self, raw_location):
        final_location = {}
        final_location['parent'] = raw_location['parent']
        final_location['lon'] = raw_location['lon']
        final_location['lat'] = raw_location['lat']
        final_location['display_name'] = raw_location['display_name']
        final_location['importance'] = raw_location['importance']
        final_location['address'] = raw_location['address']
        final_location['osm_id'] = raw_location['osm_id']
        final_location['place_id'] = raw_location['place_id']
        final_location['boundingbox'] = raw_location['boundingbox']
        final_location['polygon'] = {}
        final_location['polygon']['coordinates'] = raw_location['geojson']['coordinates'][0]
        final_location['color'] = Utils.generate_random_hex_color()
        final_location['city'] = self.city_name
        return final_location

    def get_zones_with_parent(self, main, sublist):
        self.printParent = False
        result = []
        for name in sublist:
            zone = self.get_zone(name, main)
            if zone is not None:
                location = self.__prepareLocation__(zone)
                self.logger.info("prepared suburb: %s", location)
                self.logger.info("------------------------------\n")
                result.append(location)
        return result

    def get_zones(self, search_list):
        result = []
        for name in search_list:
            self.logger.info(">>> start search for zone: %s", name)
            zone = self.get_zone(name, None)
            if zone is not None:
                location = self.__prepareLocation__(zone)
                self.logger.info("prepared suburb: %s", location)
                self.logger.info("------------------------------\n")
                result.append(location)
        return result

    def __get_bound__(self):
        boundingbox = self.get_city_boundary()
        if boundingbox is not None:
            return self.__create_view_box(boundingbox)
        else:
            return None

    def __create_view_box(self, bounded_box):
        result = []
        result.append(Point(bounded_box[0], bounded_box[3]))
        result.append(Point(bounded_box[1], bounded_box[2]))
        return result

    def __get_a_city(self):
        geolocator = Nominatim(user_agent=self.USER_AGENT, timeout=self.TIMEOUT)
        locations = geolocator.geocode(self.city_name, addressdetails=True,
                                       geometry=self.GEOJSON_GEOMETRY, exactly_one=False)
        city = None
        if locations is not None:
            for location in locations:
                if hasattr(location, 'raw') and location.raw['type'] == "city":
                    city = location.raw
        return city

    def get_city_boundary(self):
        city = self.__get_a_city()
        if city is not None:
            return city['boundingbox']
        else:
            self.logger.warning("no city found for : %s", self.city_name)
            return None

    def get_city_lat_long(self):
        city = self.__get_a_city()
        if city is not None:
            return {'lat': city['lat'], 'lon': city['lon']}
