package com.neighborhood.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.neighborhood.model.UserRequest;

public interface UserRequestsRepository extends MongoRepository<UserRequest, String> {
}
