import random
import unidecode


class Utils:

    @staticmethod
    def generate_random_hex_color():
        r = lambda: random.randint(0, 255)
        return '#%02X%02X%02X' % (r(), r(), r())

    @staticmethod
    def __concat_2lists_in_unique_list__(first, second):
        unique_list = first
        for element in second:
            if element not in unique_list:
                unique_list.append(element)
        return unique_list

    @staticmethod
    def remove_diacritics(accented_string):
        return unidecode.unidecode(accented_string)

    @staticmethod
    def remove_diacritics_from_list(list_with_diacritics):
        return list(map(Utils.remove_diacritics, list_with_diacritics))

    @staticmethod
    def filter_duplicates_in_json_list(zones):
        unique = {each['osm_id']: each for each in zones}.values()
        return list(unique)
