import unittest
from geopy.point import Point
from openstreetmap.OSMDataExtractor import OSMDataExtractor


class MyTestCase(unittest.TestCase):

    def test_get_suburb(self):
        extractor = OSMDataExtractor("București")
        suburb = extractor.get_zone("Pantelimon", "Pantelimon")
        self.assertEqual(suburb['parent'], 'Pantelimon')
        self.assertEqual(suburb['type'], 'suburb')
        self.assertIsNotNone(suburb['geojson']['coordinates'])
        self.assertEqual(suburb['geojson']['type'], "Polygon")

    def test_get_suburb_unknown_city(self):
        extractor = OSMDataExtractor("TEST")
        suburb = extractor.get_zone("Pantelimon", "Pantelimon")
        self.assertEqual(suburb['parent'], 'Pantelimon')
        self.assertEqual(suburb['type'], 'suburb')
        self.assertIsNotNone(suburb['geojson']['coordinates'])
        self.assertEqual(suburb['geojson']['type'], "Polygon")

    def test_get_neighbourhood(self):
        extractor = OSMDataExtractor("București")
        neighbourhood = extractor.get_zone("Dămăroaia", "Dămăroaia")
        self.assertEqual(neighbourhood['parent'], 'Dămăroaia')
        self.assertEqual(neighbourhood['type'], 'neighbourhood')
        self.assertIsNotNone(neighbourhood['geojson']['coordinates'])
        self.assertEqual(neighbourhood['geojson']['type'], "Polygon")

    def test_get_suburb_nothing(self):
        extractor = OSMDataExtractor("București")
        suburb = extractor.get_zone("adasd", "adasd")
        self.assertEqual(suburb, None)

    def test_prepareLocation(self):
        extractor = OSMDataExtractor("București")
        suburb = extractor.get_zone("Dristor", "Dristor")
        location = extractor.__prepareLocation__(suburb)
        self.assertIsNotNone(location['color'])
        self.assertIsNotNone(location['city'])
        self.assertIsNotNone(location['parent'])
        self.assertIsNotNone(location['lon'])
        self.assertIsNotNone(location['lat'])
        self.assertIsNotNone(location['display_name'])
        self.assertIsNotNone(location['importance'])
        self.assertIsNotNone(location['address'])
        self.assertIsNotNone(location['osm_id'])
        self.assertIsNotNone(location['place_id'])
        self.assertIsNotNone(location['boundingbox'])
        self.assertIsNotNone(location['polygon'])
        self.assertIsNotNone(location['polygon']['coordinates'])

    def test_get_zones_with_parent(self):
        extractor = OSMDataExtractor("București")
        result = extractor.get_zones_with_parent("Crangasi", ["Constructorilor", "Crangasi", "Regie"])
        self.assertEqual(len(result), 2)
        self.assertEqual(result[0]['address']['suburb'], "Crângași")
        self.assertEqual(result[1]['address']['suburb'], "Regie")

    def test_get_suburb_administrative(self):
        extractor = OSMDataExtractor("Cluj-Napoca")
        administrative = extractor.get_zone("Andrei Mureşanu", None)
        self.assertEqual(administrative['parent'], None)
        self.assertEqual(administrative['type'], 'administrative')
        self.assertIsNotNone(administrative['geojson']['coordinates'])
        self.assertEqual(administrative['geojson']['type'], "Polygon")

    def test_get_city_boundary(self):
        extractor = OSMDataExtractor("București")
        boundary = extractor.get_city_boundary()
        expected = ['44.2761414', '44.5961414', '25.9427202', '26.2627202']
        self.assertEqual(expected, boundary)

    def test_get_city_boundary_none(self):
        extractor = OSMDataExtractor("asdasd")
        boundary = extractor.get_city_boundary()
        self.assertEqual(boundary, None)

    def test__get_bound__(self):
        extractor = OSMDataExtractor("București")
        expected = [Point(44.2761414, 26.2627202), Point(44.5961414, 25.9427202)]
        result = extractor.__get_bound__()
        self.assertEqual(result, expected)

    def test__get_bound__with_unknown(self):
        extractor = OSMDataExtractor("Test")
        result = extractor.__get_bound__()
        self.assertIsNone(result)

    def test_get_city_lat_long(self):
        extractor = OSMDataExtractor("București")
        expected = {'lat': '44.4361414', 'lon': '26.1027202'}
        result = extractor.get_city_lat_long()
        self.assertEqual(expected, result)

    def test_get_city_lat_long_none(self):
        extractor = OSMDataExtractor("Test")
        result = extractor.get_city_lat_long()
        self.assertIsNone(result)


if __name__ == '__main__':
    unittest.main()
