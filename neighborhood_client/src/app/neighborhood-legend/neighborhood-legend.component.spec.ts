import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeighborhoodLegendComponent } from './neighborhood-legend.component';

describe('NeighborhoodLegendComponent', () => {
  let component: NeighborhoodLegendComponent;
  let fixture: ComponentFixture<NeighborhoodLegendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeighborhoodLegendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeighborhoodLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
