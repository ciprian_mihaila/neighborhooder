package com.neighborhood.model;

import java.util.List;

public class Polygon <T> {
	private List<T> coordinates;
	
	public List<T> getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(List<T> coordinates) {
		this.coordinates = coordinates;
	}
}
