import unittest
from openstreetmap.InputParser import InputParser


class InputParserTest(unittest.TestCase):
    def test_something(self):
        input_parser = InputParser()
        result = input_parser.read_zone_and_suburbies("data\\test_input_file")
        self.assertEqual(result, [["Cotroceni", "Cotroceni"], ["Crangasi", "Constructorilor", "Crangasi", "Regie"]])


if __name__ == '__main__':
    unittest.main()
