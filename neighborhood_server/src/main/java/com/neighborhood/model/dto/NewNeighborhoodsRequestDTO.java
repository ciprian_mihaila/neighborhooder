package com.neighborhood.model.dto;

import java.util.List;

public class NewNeighborhoodsRequestDTO {
	private String city;
	private List<String> districts;
	private String locale;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public List<String> getDistricts() {
		return districts;
	}
	public void setDistricts(List<String> districts) {
		this.districts = districts;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
}
