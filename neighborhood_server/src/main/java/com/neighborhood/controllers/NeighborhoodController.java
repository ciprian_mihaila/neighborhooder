package com.neighborhood.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.neighborhood.converters.GeoJsonConverter;
import com.neighborhood.external.services.DataCollectorRequest;
import com.neighborhood.model.City;
import com.neighborhood.model.Coordinate;
import com.neighborhood.model.GMapNeighborhood;
import com.neighborhood.model.Neighborhood;
import com.neighborhood.model.OsmNeighborhood;
import com.neighborhood.model.Polygon;
import com.neighborhood.model.UserRequest;
import com.neighborhood.model.dto.NewNeighborhoodsRequestDTO;
import com.neighborhood.model.dto.UserRequestDTO;
import com.neighborhood.repositories.CityRepository;
import com.neighborhood.repositories.NeighborhoodRepository;
import com.neighborhood.repositories.UserRequestsRepository;

@RestController
@RequestMapping("/api")
@ConfigurationProperties(prefix = "collector")
public class NeighborhoodController {
	
	@Autowired
	private NeighborhoodRepository neighborhoodRepository;
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private UserRequestsRepository userRequestsRepository;
	
	@Autowired
	DataCollectorRequest dataCollectorRequest;
	
	@RequestMapping(value = "/gmap/allneighborhoods", method = RequestMethod.GET)
	public List<GMapNeighborhood> getAllGmapNeighborhoods() {
		List<OsmNeighborhood> neighborhoodsDb =  neighborhoodRepository.findAll();
		
		List<GMapNeighborhood> resultLista = new ArrayList<GMapNeighborhood>();
		for (Neighborhood<ArrayList<String>> neighborhood : neighborhoodsDb) {
			GMapNeighborhood gmapNeighborhood = new GMapNeighborhood();
			neighborhood.setName(neighborhood.getName());
			neighborhood.setParent(neighborhood.getParent());
			
			Polygon<Coordinate> gmapPolygon = new Polygon<Coordinate>();
			gmapPolygon.setCoordinates(GeoJsonConverter.convertLatLngListToCoordinateList(neighborhood.getPolygon().getCoordinates()));
			
			gmapNeighborhood.setPolygon(gmapPolygon);
			resultLista.add(gmapNeighborhood);
		}
		
		return resultLista;
	}
	
	@RequestMapping(value = "/osm/allneighborhoods", method = RequestMethod.GET)
	public List<OsmNeighborhood> getAllOsmNeighborhoods() {
		List<OsmNeighborhood> neighborhoodsDb =  neighborhoodRepository.findAll();
		return neighborhoodsDb;
	}
	
	@RequestMapping(value = "/osm/new/neighborhood", method = RequestMethod.POST)
	public String getNewOsmNeighborhoods(@RequestBody NewNeighborhoodsRequestDTO requestBody) {
		JSONArray zones = dataCollectorRequest.getZonesForCity(requestBody);
		List<OsmNeighborhood> neighborhoods = dataCollectorRequest.prepareOsmNeighborhoodList(zones);
		neighborhoodRepository.saveAll(neighborhoods);
		City city = dataCollectorRequest.getCityDetails(requestBody);
		cityRepository.save(city);
		return  neighborhoods.size()  + " neighboorhoods saved";
	}
	
	@RequestMapping(value = "/osm/neighborhoods", method = RequestMethod.GET)
	public List<OsmNeighborhood> getOsmNeighborhoodsByCityName(@RequestParam("city") String cityName) {
		List<OsmNeighborhood> neighborhoodsDb = neighborhoodRepository.findByCity(cityName);
		System.out.println("Neighborhoods Nr: " + neighborhoodsDb.size());
		return neighborhoodsDb;
	}
	
	@RequestMapping(value = "/osm/cities", method = RequestMethod.GET)
	public List<City> getCityList() {
		return cityRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
	}
	
	@RequestMapping(value = "/osm/user/new/request", method = RequestMethod.POST)
	public UserRequest createUserRequest(@RequestBody UserRequestDTO requestBody) {
		List<String> neighborhoods = Arrays.asList(requestBody.getNeighborhoods().split(","));
		UserRequest userRequet = new UserRequest(requestBody.getName(), requestBody.getEmail(), 
				requestBody.getCity(), neighborhoods);
		return userRequestsRepository.save(userRequet);
	}

}
