export interface ICity {
    _id: string,
    name :string,
    lat:string,
    lon: string
}