package com.neighborhood.converters;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.neighborhood.model.Coordinate;

import junit.framework.TestCase;

public class GeoJsonConverterTests extends TestCase {

	private List<ArrayList<String>> coordinates = new ArrayList<ArrayList<String>>();
	
	@Override
	protected void setUp() throws Exception {
		coordinates.add(new ArrayList<String>(){{add("44.4703519");add("26.0438533");}});
		coordinates.add(new ArrayList<String>(){{add("44.4688417");add("26.0441033");}});
		coordinates.add(new ArrayList<String>(){{add("44.4674844");add("26.044773");}});
	}
	
	@Test
	public void testconvertLatLngListToCoordinateList() {
		List<Coordinate> result = GeoJsonConverter.convertLatLngListToCoordinateList(coordinates);
		
		for (int i = 0; i < coordinates.size(); i++) {
			Assert.assertEquals(result.get(i).getLng(), coordinates.get(i).get(0));
			Assert.assertEquals(result.get(i).getLat(), coordinates.get(i).get(1));
		}
	}
	
	@Test
	public void testconvertLatLngListToLngLatList() {
		List<ArrayList<String>> coordinatesBefore  = new ArrayList<ArrayList<String>>();
		coordinatesBefore.add(new ArrayList<String>(){{add("44.4703519");add("26.0438533");}});
		coordinatesBefore.add(new ArrayList<String>(){{add("44.4688417");add("26.0441033");}});
		coordinatesBefore.add(new ArrayList<String>(){{add("44.4674844");add("26.044773");}});
		
		GeoJsonConverter.convertLatLngListToLngLatList(coordinates);
		
		for (int i = 0; i < coordinates.size(); i++) {
			Assert.assertEquals(coordinatesBefore.get(i).get(0), coordinates.get(i).get(1));
			Assert.assertEquals(coordinatesBefore.get(i).get(1), coordinates.get(i).get(0));
		}
	}
}
