import unittest
import json
from api.RestAPI import app


class MyTestCase(unittest.TestCase):
    BASE_URL_SUBURBIES = 'http://127.0.0.1:5000/get/suburbies'
    BASE_URL_CITY = 'http://127.0.0.1:5000/get/citydetails'

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_get_city_suburbies_wrong_city_data(self):
        data = { "name": "Cluj-Napoca", "locale": "ro"}
        response = self.app.post(self.BASE_URL_SUBURBIES, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertTrue("Request should contain city parameter" in response.data.decode("utf-8"))

    def test_get_city_suburbies_wrong_no_data(self):
        response = self.app.post(self.BASE_URL_SUBURBIES, data=None, content_type='application/json')
        self.assertEqual(response.status_code, 400)
        #self.assertTrue("Empty data" in response.data.decode("utf-8"))

    def test_get_city_suburbies_no_locale(self):
        data = {"city": {"name": "test"}}
        response = self.app.post(self.BASE_URL_SUBURBIES, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertTrue("Request should contain locale parameters" in response.data.decode("utf-8"))

    def test_get_city_suburbies_wrong_no_name(self):
        data = {"city": {"locale": "test"}}
        response = self.app.post(self.BASE_URL_SUBURBIES, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertTrue("Request should contain name parameter" in response.data.decode("utf-8"))

    def test_get_city_suburbies(self):
        data = {"city": {
                        "name": "Cluj-Napoca",
                         "locale": "ro"
                        }
                }
        response = self.app.post(self.BASE_URL_SUBURBIES, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        #self.assertTrue("Request should contain name and bound parameters" in response.data.decode("utf-8"))

    def test_get_suburbies_with_name(self):
        data = {
                "city": {
                        "name": "Cluj-Napoca",
                        "locale": "ro"
                    }
                }
        response = self.app.post(self.BASE_URL_SUBURBIES, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue("Bulgaria" in response.data.decode("utf-8"))
        self.assertTrue("Gheorgheni" in response.data.decode("utf-8"))
        self.assertTrue("Grigorescu" in response.data.decode("utf-8"))

    def test_get_suburbies_with_name_and_districts(self):
        data = {
                "city": {
                        "name": "București",
                        "locale": "ro",
                        "districts": ['Străulești']
                    }
                }
        response = self.app.post(self.BASE_URL_SUBURBIES, data=json.dumps(data), content_type='application/json')
        print(str(response.data))
        self.assertEqual(response.status_code, 200)
        self.assertTrue("Vitan" in str(response.data))
        self.assertTrue("Pantelimon" in str(response.data))
        self.assertTrue(b"Str\\u0103ule\\u0219ti" in str(response.data))

    def test_get_city_suburbies_no_search_zones(self):
        data = {
            "city": {
                "name": "Test",
                "locale": "ro",
                "districts": ['asdadadadadasdasd', 'adsasdasdasdas', '21asdasdadasd31']
            }
        }
        response = self.app.post(self.BASE_URL_SUBURBIES, data=json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_get_city_details(self):
        response = self.app.get(self.BASE_URL_CITY + "?name=Cluj-Napoca" , content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue("{\"lat\":\"46.7782449\",\"lon\":\"23.606868698987\"}" in str(response.data))

    def test_get_city_details_no_args(self):
        response = self.app.get(self.BASE_URL_CITY, content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertTrue("Request should contain city name" in response.data.decode("utf-8"))

    def test_get_city_details_no_args(self):
        response = self.app.get(self.BASE_URL_CITY + "?name=Test", content_type='application/json')
        self.assertEqual(response.status_code, 404)
        self.assertTrue("No details found in found in OSM" in response.data.decode("utf-8"))

if __name__ == '__main__':
    unittest.main()
